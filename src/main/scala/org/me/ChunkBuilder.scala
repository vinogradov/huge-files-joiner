package org.me

import java.io.{BufferedWriter, File, FileWriter}

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

/**
  * Sorted portion of the input dataset.
  * It can be implemented with RandomAccessFile to reduce a number of intermediate files.
  *
  * @param file Intermediate file that contains a sorted portion of the input dataset
  * @param fromPosition Not used currently (reserved for RandomAccessFile based implementation)
  * @param linesNum Not used currently (reserved for RandomAccessFile based implementation)
  */
case class FileChunk(file: File, fromPosition: Long = 0, linesNum: Int = Int.MaxValue)

case class FileRecord(index: Int, data: String) {
  override def toString: String = s"$index\t$data"
}

object FileRecord extends LazyLogging {
  private val tsvRecordPattern = "(.*)\t(.*)".r

  def apply(tsvRecord: String): FileRecord = {
    val tsvRecordPattern(i, d) = tsvRecord
    new FileRecord(i.toInt, d)
  }

  def writeFile(file: File, records: List[FileRecord]): Unit = {
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      records.foreach(r => writer.write(s"${r.toString}\n"))
    } finally {
      writer.close()
    }
    logger.debug(s"Sorted dataset (${records.size} records) was written to [${file.getName}]")
  }
}

class ChunkBuilder extends LazyLogging {

  /**
    * Builds a set of sorted chunks from the input dataset.
    * Building process sequentially builds each chunk and sort it in memory.
    *
    * @param inputFile input dataset
    * @param maxChunkLines maximum number of lines in a chunk
    * @return list of sorted chunks
    */
  def buildFileChunks(inputFile: File, maxChunkLines: Long): List[FileChunk] = { // todo: change maxChunkLines to max size of intermediate data in bytes

    logger.info(s"Building sorted chunks from [${inputFile.getName}] by $maxChunkLines lines...")

    assert(maxChunkLines > 0)

    var result = List.empty[FileChunk]

    using(io.Source.fromFile(inputFile)) { source =>
      var totalChunkLines = 0L
      var currentChunkLines = 0L
      var currentFile = createTempFile(s"${inputFile.getName}-1", s"-$totalChunkLines")
      var currentLines = List.empty[FileRecord]
      source.getLines.foreach { line =>
        currentLines = FileRecord(line) :: currentLines
        totalChunkLines += 1
        currentChunkLines += 1
        if (currentChunkLines == maxChunkLines) {
          val sortedLines = currentLines.sortBy(_.index)
          FileRecord.writeFile(currentFile, sortedLines)
          result = FileChunk(currentFile) :: result
          currentLines = List.empty[FileRecord]
          currentChunkLines = 0L
          currentFile = createTempFile(s"${inputFile.getName}-1", s"-$totalChunkLines")
        }
      }
      if (currentChunkLines > 0 && currentChunkLines < maxChunkLines) {
        val sortedLines = currentLines.sortBy(_.index)
        FileRecord.writeFile(currentFile, sortedLines)
        result = FileChunk(currentFile) :: result
      }
    }
    logger.info("Building completed")

    result
  }

  def createTempFile(prefix: String, suffix: String): File = {
    var currentFile = File.createTempFile(prefix, suffix)
    logger.debug(s"Created temporary chunk file [${currentFile.getName}]")
    currentFile
  }
}
