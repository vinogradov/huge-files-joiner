package org.me

import java.io._

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

/**
  * Class to merge sorted files into one file.
  * It uses a separate execution context to run process of chunks merging concurrently.
  * All the temporal intermediate files are removed during this process.
  *
  * @param namePrefix prefix of temporal intermediate files
  * @param blockingEc separate execution context to run blocking IO processes
  */
class Merger(namePrefix: String)(blockingEc: ExecutionContext) extends LazyLogging {

  import scala.concurrent.ExecutionContext.Implicits.global

  /**
    * It merges sorted {@link org.me.FileRecord} chunks into one sorted chunk
    * It cannot work correctly in case of unsorted chunks
    *
    * @param chunks sorted list of chunks
    * @return sorted chunk
    */
  def mergeFileChunks(chunks: List[FileChunk]): Future[FileChunk] = {
    logger.info(s"Merging [${chunks.size}] sorted chunks into only one...")
    logger.debug(s"Merging [$chunks]...")

    assert(chunks.nonEmpty)

    val result = if (chunks.size == 1) Future.successful(chunks.head)
    else Future.sequence(chunks.sliding(2, 2).map(mergePair).toList).flatMap(mergeFileChunks)
    result.onComplete {
      case Success(_) =>
        logger.info("Merging completed")
        logger.debug(s"Merging [$chunks] completed")
      case _ =>
    }
    result
  }

  private def mergePair(chunks: List[FileChunk]): Future[FileChunk] = Future {
    logger.debug(s"Merging a pair of chunks [$chunks] started...")
    val emptyFile = File.createTempFile(namePrefix, "-empty")
    val (chunk1, chunk2) = (chunks.head, chunks.tail.headOption.getOrElse(FileChunk(emptyFile)))
    val resultFile = File.createTempFile(namePrefix, "-") // todo: add index to temp file suffix
    val writer = new BufferedWriter(new FileWriter(resultFile))
    val reader1 = new BufferedReader(new FileReader(chunk1.file))
    val reader2 = new BufferedReader(new FileReader(chunk2.file))
    try {
      var line1 = reader1.readLine()
      var line2 = reader2.readLine()
      var record1 = if (line1 != null) FileRecord(line1) else null // todo: optional type
      var record2 = if (line2 != null) FileRecord(line2) else null
      while (line1 != null || line2 != null) {
        while (line1 != null && (line2 == null || record1.index < record2.index)) {
          writer.write(s"${record1.toString}\n")
          line1 = reader1.readLine()
          record1 = if (line1 != null) FileRecord(line1) else null
        }
        while (line2 != null && (line1 == null || record1.index >= record2.index)) {
          writer.write(s"${record2.toString}\n")
          line2 = reader2.readLine()
          record2 = if (line2 != null) FileRecord(line2) else null
        }
      }
      logger.debug(s"Merging a pair of chunks [$chunks] completed...")
      FileChunk(resultFile)
    } finally {
      reader2.close()
      reader1.close()
      writer.close()
      chunk1.file.delete()
      chunk2.file.delete()
    }
  }(blockingEc)
}
