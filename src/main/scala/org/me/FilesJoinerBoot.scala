package org.me

import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.{Executors, TimeUnit}

import com.typesafe.scalalogging.LazyLogging
import scopt.Read

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

case class Config(threads: Int = 8, joinType: Join = Left, maxLines: Int = 100000, leftFile: File, rightFile: File, outputFile: File)

object FilesJoinerBoot extends App with LazyLogging { // todo: test it

  implicit val joinRead: Read[Join] = Read.reads(Join.apply)

  val parser = buildArgsParser

  parser.parse(args, Config(leftFile = new File(""), rightFile = new File(""), outputFile = new File(""))) match {
    case Some(config) =>
      val executor = Executors.newFixedThreadPool(config.threads)
      val blockingEc: ExecutionContext = ExecutionContext.fromExecutor(executor)
      logger.info(s"Joining two files with parameters: $config...")
      val fileJoiner = new FilesJoiner(
        config.leftFile,
        config.rightFile,
        config.maxLines,
        blockingEc
      )
      config.outputFile.createNewFile()
      logger.info(s"New output file have been created [${config.outputFile}]")
      val writer = new BufferedWriter(new FileWriter(config.outputFile))
      writer.write("id\tcity_left\tcity_right\n")
      try {
        var counter = 0
        val start = System.nanoTime
        if (config.joinType == Left) {
          logger.info(s"Running left join process...")
          Await.ready(fileJoiner.leftJoin {
            case (left, rightOpt) =>
              writer.write(s"${left.index.toString}\t${left.data}\t${rightOpt.map(_.data).getOrElse("")}\n")
              counter += 1
          }, Duration.Inf)
          val elapsedTime = System.nanoTime - start
          logger.info(s"Left join completed: [$counter] records joined, [${Duration(elapsedTime, TimeUnit.NANOSECONDS).toUnit(TimeUnit.SECONDS)}] seconds elapsed")
        } else if (config.joinType == Inner) {
          logger.info(s"Running inner join process...")
          Await.ready(fileJoiner.innerJoin {
            case (left, right) =>
              writer.write(s"${left.index.toString}\t${left.data}\t${right.data}\n")
              counter += 1
          }, Duration.Inf)
          val elapsedTime = System.nanoTime - start
          logger.info(s"Inner join completed: [$counter] records joined, [${Duration(elapsedTime, TimeUnit.NANOSECONDS).toUnit(TimeUnit.SECONDS)}] seconds elapsed")
        } else {
          throw new IllegalArgumentException(s"Unsupported join-type: ${config.joinType}")
        }
      } finally {
        writer.close()
        executor.shutdown()
      }
    case None =>
  }

  private def buildArgsParser = new scopt.OptionParser[Config]("files-joiner") {
    head("files-joiner", "0.x")

    opt[Int]('c', "threads")
      .action((x, c) => c.copy(threads = x))
      .text("Threads number of joining process")

    opt[Join]('t', "join-type")
      .action((x, c) => c.copy(joinType = x))
      .text("Type of join (left or inner)")

    opt[Int]('m', "max-lines")
      .action((x, c) => c.copy(maxLines = x))
      .text("Maximum number of lines to sort in memory")

    opt[File]('l', "left-file")
      .required()
      .valueName("<file>")
      .action((x, c) => c.copy(leftFile = x))
      .text("Left file")

    opt[File]('r', "right-file")
      .required()
      .valueName("<file>")
      .action((x, c) => c.copy(rightFile = x))
      .text("Right file")

    opt[File]('o', "output-file")
      .required()
      .valueName("<file>")
      .action((x, c) => c.copy(outputFile = x))
      .text("Output file")
  }

}

sealed trait Join
object Join {
  def apply(s: String): Join = s match {
    case "left" => Left
    case "inner" => Inner
    case _ => throw new IllegalArgumentException("'left' or 'inner' only supported")
  }
}
case object Left extends Join
case object Inner extends Join
