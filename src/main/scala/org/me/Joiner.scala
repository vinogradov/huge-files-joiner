package org.me

/**
  * Class to join two sets of records sorted by index field.
  * It cannot correctly works with unsorted sets.
  *
  * @param leftIterator sorted left set of records
  * @param rightIterator sorted right set of records
  */
class Joiner(leftIterator: Iterator[FileRecord], rightIterator: Iterator[FileRecord]) {

  /**
    * It implements left join functionality.
    * @param f a function to run action on each joined pair
    */
  def leftJoinSortedChunks(f: (FileRecord, Option[FileRecord]) => Unit): Unit = { // todo: implement as a stream
    var leftRecordOpt = if (leftIterator.hasNext) Some(leftIterator.next) else None
    var rightRecordOpt = if (rightIterator.hasNext) Some(rightIterator.next) else None
    while (leftRecordOpt.isDefined) {
      if (rightRecordOpt.isDefined) {
        if (leftRecordOpt.get.index == rightRecordOpt.get.index) {
          f(leftRecordOpt.get, rightRecordOpt)
          leftRecordOpt = if (leftIterator.hasNext) Some(leftIterator.next) else None
          rightRecordOpt = if (rightIterator.hasNext) Some(rightIterator.next) else None
        } else if (leftRecordOpt.get.index < rightRecordOpt.get.index) {
          f(leftRecordOpt.get, None)
          leftRecordOpt = if (leftIterator.hasNext) Some(leftIterator.next) else None
        } else if (leftRecordOpt.get.index > rightRecordOpt.get.index) {
          rightRecordOpt = if (rightIterator.hasNext) Some(rightIterator.next) else None
        }
      } else {
        f(leftRecordOpt.get, None)
        leftRecordOpt = if (leftIterator.hasNext) Some(leftIterator.next) else None
      }
    }
  }

  /**
    * It implements inner join functionality.
    * @param f a function to run action on each joined pair
    */
  def innerJoinSortedChunks(f: (FileRecord, FileRecord) => Unit): Unit = { // todo: implement as a stream
    var leftRecord = if (leftIterator.hasNext) leftIterator.next else return
    var rightRecord = if (rightIterator.hasNext) rightIterator.next else return
    while (true) {
      if (leftRecord.index == rightRecord.index) {
        f(leftRecord, rightRecord)
        leftRecord = if (leftIterator.hasNext) leftIterator.next else return
        rightRecord = if (rightIterator.hasNext) rightIterator.next else return
      } else if (leftRecord.index < rightRecord.index) {
        leftRecord = if (leftIterator.hasNext) leftIterator.next else return
      } else if (leftRecord.index > rightRecord.index) {
        rightRecord = if (rightIterator.hasNext) rightIterator.next else return
      }
    }
  }
}
