package org

package object me {
  def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
    try {
      f(resource)
    } finally {
      resource.close()
    }

  def using2[A <: { def close(): Unit }, B](resource1: A, resource2: A)(f: (A, A) => B): B =
    try {
      f(resource1, resource2)
    } finally {
      resource1.close()
      resource2.close()
    }
}
