package org.me

import java.io.File

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}
import scala.io.BufferedSource

class FilesJoiner(leftFile: File, rightFile: File, maxLinesInMemory: Int, blockingEc: ExecutionContext) extends LazyLogging {

  // todo: can be potentially failed in case of very huge files and maxLinesInMemory about 1-,10-,100-,1000-
  // todo: (a lot of intermediate files will be created in that case)

  import scala.concurrent.ExecutionContext.Implicits.global

  def leftJoin(f: (FileRecord, Option[FileRecord]) => Unit): Future[Unit] = prepareJoin {
    case (leftSource, rightSource) =>
      new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).leftJoinSortedChunks(f)
  }

  def innerJoin(f: (FileRecord, FileRecord) => Unit): Future[Unit] = prepareJoin {
    case (leftSource, rightSource) =>
      new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).innerJoinSortedChunks(f)
  }

  private def prepareJoin(f: (BufferedSource, BufferedSource) => Unit) = {
    val chunkBuilder = new ChunkBuilder
    val leftChunks = Future(chunkBuilder.buildFileChunks(leftFile, maxLinesInMemory))(blockingEc)
    val leftMerger = new Merger(leftFile.getName)(blockingEc)
    val rightChunks = Future(chunkBuilder.buildFileChunks(rightFile, maxLinesInMemory))(blockingEc)
    val rightMerger = new Merger(rightFile.getName)(blockingEc)
    val leftMergedF = leftChunks.flatMap(leftMerger.mergeFileChunks)
    val rightMergedF = rightChunks.flatMap(rightMerger.mergeFileChunks)
    for {
      leftMerged <- leftMergedF
      rightMerged <- rightMergedF
    } yield {
      using2(io.Source.fromFile(leftMerged.file), io.Source.fromFile(rightMerged.file))(f)
      leftMerged.file.deleteOnExit()
      rightMerged.file.deleteOnExit()
    }
  }
}
