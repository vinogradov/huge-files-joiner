package org.me

import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.{Executors, TimeUnit}

import com.github.tototoshi.csv.CSVReader
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable
import scala.concurrent.{Await, ExecutionContext}
import scala.util.Random
import scala.concurrent.duration._

class TestDataBuilder extends FlatSpec with Matchers {

  private val multipleThreadsEc = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(8))
  private val singleThreadEc = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  behavior of "TestDataBuilder"

  ignore should "join big files" in {
    val originsTsv = new File("/Users/vinogradov/Downloads/origins.tsv")
    val destinationsTsv = new File("/Users/vinogradov/Downloads/destinations.tsv")

    val filesJoiner = new FilesJoiner(originsTsv, destinationsTsv, 10000, multipleThreadsEc)
    val start = System.nanoTime
    Await.ready(filesJoiner.innerJoin {
      case (left, right) =>
    }, Duration.Inf)
    val elapsedTime = System.nanoTime - start
    println(s"Elapsed time: ${Duration(elapsedTime, TimeUnit.NANOSECONDS).toUnit(TimeUnit.SECONDS)}")
  }

  ignore should "prepare test dataset" in {
    // original file was downloaded from https://www.transtats.bts.gov/DL_SelectFields.asp?Table_ID=236&DB_Short_Name=On-Time
    var foreignKey = 0
    val originRecords = mutable.ListBuffer.empty[FileRecord]
    val destinationRecords = mutable.ListBuffer.empty[FileRecord]

    for(n <- Range(1,10)) {
      val reader = CSVReader.open(new File("/Users/vinogradov/Downloads/68423122_T_ONTIME_REPORTING.csv"))
      var currentOriginId = 0
      var currentDestinationId = 0
      reader.foreach {
        case originId :: originCityName :: destinationId :: destinationCityName :: tail =>
          if (originId.toInt != currentOriginId || destinationId.toInt != currentDestinationId) {
            foreignKey += 1
            currentOriginId = originId.toInt
            currentDestinationId = destinationId.toInt
            originRecords += FileRecord(s"$foreignKey\t${parseCityName(originCityName)}-$n")
            destinationRecords += FileRecord(s"$foreignKey\t${parseCityName(destinationCityName)}-$n")
          }
      }
      reader.close()
    }

    val originsTsv = new File("/Users/vinogradov/Downloads/origins.tsv")
    originsTsv.createNewFile()
    val destinationsTsv = new File("/Users/vinogradov/Downloads/destinations.tsv")
    destinationsTsv.createNewFile()

    prepareTestData(originsTsv, Random.shuffle(originRecords.toList))
    prepareTestData(destinationsTsv, Random.shuffle(destinationRecords.toList))
  }

  private val cityNamePattern = """([A-Za-z/\\\s-.]+), [A-Z]{2}""".r
  private def parseCityName(cityName: String): String = {
    val cityNamePattern(name) = cityName
    name
  }

  private def prepareTestData(file: File, testRecords: List[FileRecord]): File = {
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      testRecords.foreach(r => writer.write(s"${r.toString}\n"))
    } finally {
      writer.close()
    }
    file
  }
}
