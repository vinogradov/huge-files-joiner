package org.me

import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.Executors

import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class MergerTest extends FlatSpec with Matchers {

  private val multipleThreadsEc = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4)) // todo: use in test
  private val singleThreadEc = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  behavior of "Merger"

  it should "correctly merge chunks" in {
    val testData = List( // todo: generate data automatically
      FileRecord(12, "Osaka"),
      FileRecord(16, "Izmir"),
      FileRecord(4, "Paris"),
      FileRecord(22, "Omsk"),
      FileRecord(15, "London"),
      FileRecord(1, "New York"),
      FileRecord(18, "Minsk"),
      FileRecord(9, "Saint-Petersburg"),
      FileRecord(5, "Berlin"),
      FileRecord(3, "Tokyo"),
      FileRecord(7, "Riga"),
      FileRecord(11, "Shanghai"),
      FileRecord(19, "Manila"),
      FileRecord(20, "Montreal"),
      FileRecord(6, "Rome"),
      FileRecord(2, "Moscow"),
      FileRecord(10, "Madrid"),
      FileRecord(14, "Delhi"),
      FileRecord(8, "Mexico"),
      FileRecord(21, "Munich"),
      FileRecord(13, "Cairo"),
      FileRecord(17, "Toronto")
    )
    val inputFile = prepareTestData(testData)

    val chunkBuilder = new ChunkBuilder

    val inputChunks = chunkBuilder.buildFileChunks(inputFile, maxChunkLines = 2)
    inputChunks.foreach(_.file.deleteOnExit())

    val merger = new Merger(inputFile.getName)(singleThreadEc)
    val resultChunk = Await.result(merger.mergeFileChunks(inputChunks), 5.seconds)
    resultChunk.file.deleteOnExit()
    val result = readFileRecords(resultChunk)

    result.sortBy(_.index) should be (testData.sortBy(_.index))
  }

  it should "correctly work with minimal set" in {
    val testData = List(
      FileRecord(2, "Moscow")
    )
    val inputFile = prepareTestData(testData)

    val chunkBuilder = new ChunkBuilder

    val inputChunks = chunkBuilder.buildFileChunks(inputFile, maxChunkLines = 2)
    inputChunks.foreach(_.file.deleteOnExit())

    val merger = new Merger(inputFile.getName)(singleThreadEc)
    val resultChunk = Await.result(merger.mergeFileChunks(inputChunks), 5.seconds)
    resultChunk.file.deleteOnExit()
    val result = readFileRecords(resultChunk)

    result should be (testData)
  }

  it should "throw assertion error with zero set" in {
    val merger = new Merger("joiner-test-zero")(singleThreadEc)
    an[AssertionError] should be thrownBy Await.result(merger.mergeFileChunks(List.empty[FileChunk]), 5.seconds)
  }

  private def readFileRecords(chunk: FileChunk): List[FileRecord] =
    using(io.Source.fromFile(chunk.file))(_.getLines.map(FileRecord.apply).toList)

  private def prepareTestData(testRecords: List[FileRecord]): File = {
    val file = File.createTempFile("joiner-test-", "")
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      testRecords.foreach(r => writer.write(s"${r.toString}\n"))
    } finally {
      writer.close()
    }
    file
  }
}
