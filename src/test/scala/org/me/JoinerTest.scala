package org.me

import java.io.{BufferedWriter, File, FileWriter}

import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable

class JoinerTest extends FlatSpec with Matchers {

  behavior of "Joiner"

  it should "correctly process join with empty left dataset" in {
    val testDataLeft = List.empty[FileRecord]
    val fileLeft = prepareTestData(testDataLeft)
    val testDataRight = List(
      FileRecord(1, "Shanghai"),
      FileRecord(2, "Osaka"),
      FileRecord(4, "Delhi"),
      FileRecord(5, "London"),
      FileRecord(7, "Berlin"),
      FileRecord(15, "New York")
    )
    val fileRight = prepareTestData(testDataRight)

    val leftJoinExpectedResult = List.empty[(FileRecord, Option[FileRecord])]

    val leftJoinActualResult = mutable.ListBuffer.empty[(FileRecord, Option[FileRecord])]
    using2(io.Source.fromFile(fileLeft), io.Source.fromFile(fileRight)) {
      case (leftSource, rightSource) =>
        new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).leftJoinSortedChunks {
          case (left, rightOpt) => leftJoinActualResult += Tuple2(left, rightOpt)
        }
    }

    leftJoinActualResult.toList should be(leftJoinExpectedResult)

    val innerJoinExpectedResult = List.empty[(FileRecord, FileRecord)]

    val innerJoinActualResult = mutable.ListBuffer.empty[(FileRecord, FileRecord)]
    using2(io.Source.fromFile(fileLeft), io.Source.fromFile(fileRight)) {
      case (leftSource, rightSource) =>
        new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).innerJoinSortedChunks {
          case (left, right) => innerJoinActualResult += Tuple2(left, right)
        }
    }

    innerJoinActualResult.toList should be(innerJoinExpectedResult)
  }

  it should "correctly process join with empty right dataset" in {
    val testDataLeft = List(
      FileRecord(1, "New York"),
      FileRecord(2, "Moscow"),
      FileRecord(3, "Tokyo"),
      FileRecord(4, "Paris"),
      FileRecord(5, "Berlin"),
      FileRecord(10, "Madrid"),
      FileRecord(15, "Omsk"),
      FileRecord(16, "Novosibirsk"),
      FileRecord(17, "Athens")
    )
    val fileLeft = prepareTestData(testDataLeft)

    val testDataRight = List.empty[FileRecord]
    val fileRight = prepareTestData(testDataRight)

    val leftJoinExpectedResult = List(
      (FileRecord(1, "New York"), None),
      (FileRecord(2, "Moscow"), None),
      (FileRecord(3, "Tokyo"), None),
      (FileRecord(4, "Paris"), None),
      (FileRecord(5, "Berlin"), None),
      (FileRecord(10, "Madrid"), None),
      (FileRecord(15, "Omsk"), None),
      (FileRecord(16, "Novosibirsk"), None),
      (FileRecord(17, "Athens"), None)
    )

    val leftJoinActualResult = mutable.ListBuffer.empty[(FileRecord, Option[FileRecord])]
    using2(io.Source.fromFile(fileLeft), io.Source.fromFile(fileRight)) {
      case (leftSource, rightSource) =>
        new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).leftJoinSortedChunks {
          case (left, rightOpt) => leftJoinActualResult += Tuple2(left, rightOpt)
        }
    }

    leftJoinActualResult.toList should be(leftJoinExpectedResult)

    val innerJoinExpectedResult = List.empty[(FileRecord, FileRecord)]

    val innerJoinActualResult = mutable.ListBuffer.empty[(FileRecord, FileRecord)]
    using2(io.Source.fromFile(fileLeft), io.Source.fromFile(fileRight)) {
      case (leftSource, rightSource) =>
        new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).innerJoinSortedChunks {
          case (left, right) => innerJoinActualResult += Tuple2(left, right)
        }
    }

    innerJoinActualResult.toList should be(innerJoinExpectedResult)
  }

  it should "correctly process join datasets of different sizes" in {
    val testDataLeft = List(
      FileRecord(1, "New York"),
      FileRecord(2, "Moscow"),
      FileRecord(3, "Tokyo"),
      FileRecord(4, "Paris"),
      FileRecord(5, "Berlin"),
      FileRecord(10, "Madrid"),
      FileRecord(15, "Omsk"),
      FileRecord(16, "Novosibirsk"),
      FileRecord(17, "Athens")
    )
    val fileLeft = prepareTestData(testDataLeft)

    val testDataRight = List(
      FileRecord(1, "Shanghai"),
      FileRecord(2, "Osaka"),
      FileRecord(4, "Delhi"),
      FileRecord(5, "London"),
      FileRecord(7, "Berlin"),
      FileRecord(15, "New York")
    )
    val fileRight = prepareTestData(testDataRight)

    val leftJoinExpectedResult = List(
      (FileRecord(1, "New York"), Some(FileRecord(1, "Shanghai"))),
      (FileRecord(2, "Moscow"), Some(FileRecord(2, "Osaka"))),
      (FileRecord(3, "Tokyo"), None),
      (FileRecord(4, "Paris"), Some(FileRecord(4, "Delhi"))),
      (FileRecord(5, "Berlin"), Some(FileRecord(5, "London"))),
      (FileRecord(10, "Madrid"), None),
      (FileRecord(15, "Omsk"), Some(FileRecord(15, "New York"))),
      (FileRecord(16, "Novosibirsk"), None),
      (FileRecord(17, "Athens"), None)
    )

    val leftJoinActualResult = mutable.ListBuffer.empty[(FileRecord, Option[FileRecord])]
    using2(io.Source.fromFile(fileLeft), io.Source.fromFile(fileRight)) {
      case (leftSource, rightSource) =>
        new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).leftJoinSortedChunks {
          case (left, rightOpt) => leftJoinActualResult += Tuple2(left, rightOpt)
        }
    }

    leftJoinActualResult.toList should be(leftJoinExpectedResult)

    val innerJoinExpectedResult = List(
      (FileRecord(1, "New York"), FileRecord(1, "Shanghai")),
      (FileRecord(2, "Moscow"), FileRecord(2, "Osaka")),
      (FileRecord(4, "Paris"), FileRecord(4, "Delhi")),
      (FileRecord(5, "Berlin"), FileRecord(5, "London")),
      (FileRecord(15, "Omsk"), FileRecord(15, "New York"))
    )

    val innerJoinActualResult = mutable.ListBuffer.empty[(FileRecord, FileRecord)]
    using2(io.Source.fromFile(fileLeft), io.Source.fromFile(fileRight)) {
      case (leftSource, rightSource) =>
        new Joiner(leftSource.getLines.map(FileRecord.apply), rightSource.getLines.map(FileRecord.apply)).innerJoinSortedChunks {
          case (left, right) => innerJoinActualResult += Tuple2(left, right)
        }
    }

    innerJoinActualResult.toList should be(innerJoinExpectedResult)
  }

  private def prepareTestData(testRecords: List[FileRecord]): File = {
    val file = File.createTempFile("joiner-test-", "")
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      testRecords.foreach(r => writer.write(s"${r.toString}\n"))
    } finally {
      writer.close()
    }
    file
  }
}
