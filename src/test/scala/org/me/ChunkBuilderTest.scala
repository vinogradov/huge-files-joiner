package org.me

import java.io.{BufferedWriter, File, FileNotFoundException, FileWriter}

import org.scalatest.{FlatSpec, Matchers}

class ChunkBuilderTest extends FlatSpec with Matchers {

  behavior of "ChunksBuilder"

  it should "correctly built file chunks" in {
    val testData = List( // todo: generate data automatically
      FileRecord(12, "Osaka"),
      FileRecord(16, "Izmir"),
      FileRecord(5, "Berlin"),
      FileRecord(4, "Paris"),
      FileRecord(22, "Omsk"),
      FileRecord(15, "London"),
      FileRecord(1, "New York"),
      FileRecord(18, "Minsk"),
      FileRecord(3, "Tokyo"),
      FileRecord(19, "Manila"),
      FileRecord(6, "Rome"),
      FileRecord(10, "Madrid"),
      FileRecord(9, "Saint-Petersburg"),
      FileRecord(11, "Shanghai"),
      FileRecord(14, "Delhi"),
      FileRecord(20, "Montreal"),
      FileRecord(7, "Riga"),
      FileRecord(8, "Mexico"),
      FileRecord(21, "Munich"),
      FileRecord(2, "Moscow"),
      FileRecord(13, "Cairo"),
      FileRecord(17, "Toronto")
    )
    val inputFile = prepareTestData(testData)

    val chunkBuilder = new ChunkBuilder

    val filesBy1 = chunkBuilder.buildFileChunks(inputFile, maxChunkLines = 1)
    val filesBy2 = chunkBuilder.buildFileChunks(inputFile, maxChunkLines = 2)
    val filesBy5 = chunkBuilder.buildFileChunks(inputFile, maxChunkLines = 5)
    filesBy1.foreach(_.file.deleteOnExit())
    filesBy2.foreach(_.file.deleteOnExit())
    filesBy5.foreach(_.file.deleteOnExit())

    filesBy1.size should be(chunksCount(testData.size, 1))
    filesBy2.size should be(chunksCount(testData.size, 2))
    filesBy5.size should be(chunksCount(testData.size, 5))

    val dataBy1 = filesBy1.map(readFileRecords)
    val dataBy2 = filesBy2.map(readFileRecords)
    val dataBy5 = filesBy5.map(readFileRecords)

    dataBy1.foreach(checkSort)
    dataBy2.foreach(checkSort)
    dataBy5.foreach(checkSort)

    dataBy1.flatten.sortBy(_.index) should be(testData.sortBy(_.index))
    dataBy2.flatten.sortBy(_.index) should be(testData.sortBy(_.index))
    dataBy5.flatten.sortBy(_.index) should be(testData.sortBy(_.index))
  }

  it should "correctly built file chunks from minimal dataset" in {
    val testData = List(
      FileRecord(2, "Moscow")
    )
    val inputFile = prepareTestData(testData)

    val chunkBuilder = new ChunkBuilder

    val chunk = chunkBuilder.buildFileChunks(inputFile, maxChunkLines = 10)
    chunk.foreach(_.file.deleteOnExit())

    chunk.size should be(1)

    val data = chunk.map(readFileRecords)

    data.flatten should be(testData)
  }

  it should "correctly built file chunks from empty dataset" in {
    val emptyTestData = List.empty[FileRecord]
    val emptyInputFile = prepareTestData(emptyTestData)

    val chunkBuilder = new ChunkBuilder

    val emptyChunk = chunkBuilder.buildFileChunks(emptyInputFile, maxChunkLines = 10)
    emptyChunk.foreach(_.file.deleteOnExit())

    emptyChunk.size should be(0)

    val emptyData = emptyChunk.map(readFileRecords)

    emptyData.flatten should be(List.empty[FileRecord])
  }

  it should "throw 'maxChunkLines > 0' assertion error" in {
    val chunkBuilder = new ChunkBuilder
    an[AssertionError] should be thrownBy chunkBuilder.buildFileChunks(new File("assert maxChunkLines > 0"), maxChunkLines = 0)
  }

  it should "throw 'FileNotFound' error" in {
    val chunkBuilder = new ChunkBuilder
    an[FileNotFoundException] should be thrownBy chunkBuilder.buildFileChunks(new File("file_not_found"), maxChunkLines = 1)
  }

  private def readFileRecords(chunk: FileChunk): List[FileRecord] =
    using(io.Source.fromFile(chunk.file))(_.getLines.map(FileRecord.apply).toList)

  private def prepareTestData(testRecords: List[FileRecord]): File = {
    val file = File.createTempFile("joiner-test-", "")
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      testRecords.foreach(r => writer.write(s"${r.toString}\n"))
    } finally {
      writer.close()
    }
    file
  }

  private def checkSort(records: List[FileRecord]) = {
    records should be(records.sortBy(_.index))
  }

  private def chunksCount(testDataSize: Int, maxChunkSize: Int): Int = {
    val wholeChunks = testDataSize / maxChunkSize
    if (testDataSize % maxChunkSize > 0) wholeChunks + 1 else wholeChunks
  }
}
