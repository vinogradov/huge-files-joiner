package org.me

import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.Executors

import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class FilesJoinerTest extends FlatSpec with Matchers {

  private val multipleThreadsEc = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4))
  private val singleThreadEc = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  behavior of "FilesJoiner"

  it should "innerJoin" in {
    val testDataLeft = List(
      FileRecord(4, "Paris"),
      FileRecord(2, "Moscow"),
      FileRecord(17, "Athens"),
      FileRecord(15, "Omsk"),
      FileRecord(1, "New York"),
      FileRecord(16, "Novosibirsk"),
      FileRecord(5, "Berlin"),
      FileRecord(10, "Madrid"),
      FileRecord(3, "Tokyo")
    )
    val fileLeft = prepareTestData(testDataLeft)

    val testDataRight = List(
      FileRecord(15, "New York"),
      FileRecord(5, "London"),
      FileRecord(2, "Osaka"),
      FileRecord(7, "Berlin"),
      FileRecord(4, "Delhi"),
      FileRecord(1, "Shanghai")
    )
    val fileRight = prepareTestData(testDataRight)

    val leftJoinExpectedResult = List(
      (FileRecord(1, "New York"), Some(FileRecord(1, "Shanghai"))),
      (FileRecord(2, "Moscow"), Some(FileRecord(2, "Osaka"))),
      (FileRecord(3, "Tokyo"), None),
      (FileRecord(4, "Paris"), Some(FileRecord(4, "Delhi"))),
      (FileRecord(5, "Berlin"), Some(FileRecord(5, "London"))),
      (FileRecord(10, "Madrid"), None),
      (FileRecord(15, "Omsk"), Some(FileRecord(15, "New York"))),
      (FileRecord(16, "Novosibirsk"), None),
      (FileRecord(17, "Athens"), None)
    )

    val leftJoinActualResult = mutable.ListBuffer.empty[(FileRecord, Option[FileRecord])]

    val filesJoiner = new FilesJoiner(fileLeft, fileRight, 1, multipleThreadsEc)
    Await.ready(filesJoiner.leftJoin {
      case (left, rightOpt) => leftJoinActualResult += Tuple2(left, rightOpt)
    }, 5.seconds)

    leftJoinActualResult.toList should be(leftJoinExpectedResult)
  }

  it should "leftJoin" in {
    val testDataLeft = List(
      FileRecord(4, "Paris"),
      FileRecord(2, "Moscow"),
      FileRecord(17, "Athens"),
      FileRecord(15, "Omsk"),
      FileRecord(5, "Berlin"),
      FileRecord(1, "New York"),
      FileRecord(16, "Novosibirsk"),
      FileRecord(10, "Madrid"),
      FileRecord(3, "Tokyo")
    )
    val fileLeft = prepareTestData(testDataLeft)

    val testDataRight = List(
      FileRecord(2, "Osaka"),
      FileRecord(15, "New York"),
      FileRecord(7, "Berlin"),
      FileRecord(5, "London"),
      FileRecord(1, "Shanghai"),
      FileRecord(4, "Delhi")
    )
    val fileRight = prepareTestData(testDataRight)

    val innerJoinExpectedResult = List(
      (FileRecord(1, "New York"), FileRecord(1, "Shanghai")),
      (FileRecord(2, "Moscow"), FileRecord(2, "Osaka")),
      (FileRecord(4, "Paris"), FileRecord(4, "Delhi")),
      (FileRecord(5, "Berlin"), FileRecord(5, "London")),
      (FileRecord(15, "Omsk"), FileRecord(15, "New York"))
    )

    val innerJoinActualResult = mutable.ListBuffer.empty[(FileRecord, FileRecord)]

    val filesJoiner = new FilesJoiner(fileLeft, fileRight, 1, multipleThreadsEc)
    Await.ready(filesJoiner.innerJoin {
      case (left, right) => innerJoinActualResult += Tuple2(left, right)
    }, 5.seconds)

    innerJoinActualResult.toList should be(innerJoinExpectedResult)
  }

  private def prepareTestData(testRecords: List[FileRecord]): File = {
    val file = File.createTempFile("joiner-test-", "")
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      testRecords.foreach(r => writer.write(s"${r.toString}\n"))
    } finally {
      writer.close()
    }
    file
  }
}
