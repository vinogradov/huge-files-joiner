name := "hugeFilesJoiner"

version := "0.1"

scalaVersion := "2.12.8"

mainClass in assembly := Some("org.me.FilesJoinerBoot")

assemblyJarName in assembly := "huge-files-joiner.jar"

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "com.github.scopt" %% "scopt" % "4.0.0-RC2",
  "com.github.tototoshi" %% "scala-csv" % "1.3.5" % Test,
  "org.scalatest" %% "scalatest" % "3.0.0" % Test
)
